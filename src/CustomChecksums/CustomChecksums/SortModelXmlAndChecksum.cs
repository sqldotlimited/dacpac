﻿using System.Linq;
using System.Xml.Linq;

namespace CustomChecksums
{
    class SortModelXmlAndChecksum
    {
        public static void RemoveSortChecksum(System.IO.FileInfo InputFile, Options args)
        {
            try
            {
                System.IO.Packaging.Package dacpac = System.IO.Packaging.Package.Open(InputFile.FullName, System.IO.FileMode.OpenOrCreate);
                System.Uri ModelUri = System.IO.Packaging.PackUriHelper.CreatePartUri(new System.Uri(Constants.ModelXmlUri, System.UriKind.Relative));
                System.IO.Packaging.PackagePart ModelPackagePart = dacpac.GetPart(ModelUri);

                // Create an XDocument so we can grab the Checksums Element from the Origin package part.
                System.Xml.Linq.XDocument ModelXml = System.Xml.Linq.XDocument.Load(System.Xml.XmlReader.Create(ModelPackagePart.GetStream()));
                // Assign the xml name space correctly.
                System.Xml.Linq.XNamespace xmlns = Constants.xmlns;

                // Remove the FileName and AssemblySymbolsName attributes as build agents can have different folder locations.
                if (args.Verbose)
                {
                    System.Console.WriteLine(string.Format("Removing /DataSchemaModel/Header/CustomData/Category/Metadata/[@Name=\"FileName\"]..."));
                    System.Console.WriteLine(string.Format("Removing /DataSchemaModel/Header/CustomData/Category/Metadata/[@Name=\"AssemblySymbolsName\"]..."));
                }
                ModelXml.Root.Elements(xmlns + "Header")
                             .Elements(xmlns + "CustomData")
                             .Elements(xmlns + "Metadata")
                             .Where(x => x.Attribute("Name").Value == "FileName" || x.Attribute("Name").Value == "AssemblySymbolsName")
                             .Remove();

                if (args.Verbose)
                {
                    System.Console.WriteLine("Sorting /DataSchemaModel/Header/CustomData/@Category...");
                }
                ModelXml.Root.Elements(xmlns + "Header")
                             .Elements(xmlns + "CustomData")
                             .OrderBy(x => (string)x.Attribute("Category").Value)
                             .ThenBy(x => (string)x.Attribute("Type")?.Value);

                if (args.Verbose)
                {
                    System.Console.WriteLine("Sorting /DataSchemaModel/Model/Element/@Type...");
                }
                ModelXml.Root.Elements(xmlns + "Model")
                             .Elements(xmlns + "Element")
                             .OrderBy(x => (string)x.Attribute("Type").Value)
                             .ThenBy(x => (string)x.Attribute("Name")?.Value)
                             .ThenBy(x => (string)x.Attribute("Disambiguator")?.Value)
                             ;

                System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                ModelXml.Save(memoryStream);
                memoryStream.Position = 0;

                byte[] byteArray = Checksum.CalculateChecksum(memoryStream);
                string readableByteArray = string.Concat(byteArray.Select(a => a.ToString("X2")));

                if (args.Verbose)
                {
                    System.Console.WriteLine("Sorted Model Checksum Generated: {0}", readableByteArray);
                }

                System.Uri ChecksumsUri = System.IO.Packaging.PackUriHelper.CreatePartUri(new System.Uri(Constants.ChecksumsUri, System.UriKind.Relative));
                System.IO.Packaging.PackagePart ChecksumsPackagePart = dacpac.GetPart(ChecksumsUri);

                // Create an XDocument so we can grab the Checksums Element from the Origin package part.
                System.Xml.Linq.XDocument ChecksumXml = System.Xml.Linq.XDocument.Load(System.Xml.XmlReader.Create(ChecksumsPackagePart.GetStream()));

                System.Xml.Linq.XElement _checksumElement = new System.Xml.Linq.XElement(xmlns + "Checksum");
                _checksumElement.Add(new System.Xml.Linq.XAttribute("Uri", Constants.SortedModelXmlUri));
                _checksumElement.SetValue(readableByteArray);

                if (args.Verbose)
                {
                    System.Console.WriteLine("Adding {0} to {1}...", _checksumElement.ToString(), Constants.ChecksumsUri);
                }

                ChecksumXml.Root.Elements(xmlns + "Checksum")
                                .LastOrDefault()
                                .AddAfterSelf(_checksumElement);

                //Grab the stream so we can save to the checksums.xml file.
                System.IO.Stream _stream = ChecksumsPackagePart.GetStream(System.IO.FileMode.Open, System.IO.FileAccess.Write);

                if (args.Verbose)
                {
                    System.Console.WriteLine(string.Format("Saving Package Part {0}...", Constants.ChecksumsUri));
                }
                ChecksumXml.Save(_stream, System.Xml.Linq.SaveOptions.None);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw ex;
            }
        }

    }
}

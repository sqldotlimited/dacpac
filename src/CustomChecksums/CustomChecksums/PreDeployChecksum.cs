﻿using System.Linq;

namespace CustomChecksums
{
    class PreDeployChecksum
    {
        /// <summary>
        /// Method that will take the predeploy.sql file and perform a checksum over the stream and save it 
        /// to the checksums.xml file within the dacpac
        /// </summary>
        /// <param name="InputFile">FileInfo of the InputFile (dacpac)</param>
        /// <param name="args">Options</param>
        public static void CreatePreDeployChecksumElement(System.IO.FileInfo InputFile, Options args)
        {
            try
            {
                System.IO.Packaging.Package dacpac = System.IO.Packaging.Package.Open(InputFile.FullName, System.IO.FileMode.OpenOrCreate);

                System.Uri PreDeployUri = System.IO.Packaging.PackUriHelper.CreatePartUri(new System.Uri(Constants.PreDeployUri, System.UriKind.Relative));
                System.IO.Packaging.PackagePart PreDeployPackagePart;

                System.IO.Packaging.PackagePartCollection _parts;
                bool _IsPreDeployFilePresent = false;

                try
                {
                    _parts = dacpac.GetParts();

                    foreach (System.IO.Packaging.PackagePart _part in _parts)
                    {
                        if (_part.Uri.ToString() == Constants.PreDeployUri)
                        {
                            if (args.Verbose)
                            {
                                System.Console.WriteLine(string.Format("{0} file found within the dacpac...", Constants.PreDeployUri));
                            }
                            _IsPreDeployFilePresent = true;
                            break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }

                if (_IsPreDeployFilePresent == true)
                {
                    PreDeployPackagePart = dacpac.GetPart(PreDeployUri);

                    byte[] byteArray = Checksum.CalculateChecksum(PreDeployPackagePart.GetStream());
                    string readableByteArray = string.Concat(byteArray.Select(a => a.ToString("X2")));

                    if (args.Verbose)
                    {
                        System.Console.WriteLine("Pre Deployment Checksum Generated: {0}", readableByteArray);
                    }

                    System.Uri ChecksumsUri = System.IO.Packaging.PackUriHelper.CreatePartUri(new System.Uri(Constants.ChecksumsUri, System.UriKind.Relative));
                    System.IO.Packaging.PackagePart ChecksumsPackagePart = dacpac.GetPart(ChecksumsUri);

                    // Create an XDocument so we can grab the Checksums Element from the Origin package part.
                    System.Xml.Linq.XDocument ChecksumXml = System.Xml.Linq.XDocument.Load(System.Xml.XmlReader.Create(ChecksumsPackagePart.GetStream()));
                    // Assign the xml name space correctly.
                    System.Xml.Linq.XNamespace xmlns = Constants.xmlns;

                    System.Xml.Linq.XElement _checksumElement = new System.Xml.Linq.XElement(xmlns + "Checksum");
                    _checksumElement.Add(new System.Xml.Linq.XAttribute("Uri", Constants.PreDeployUri));
                    _checksumElement.SetValue(readableByteArray);

                    if (args.Verbose)
                    {
                        System.Console.WriteLine("Adding {0} to {1}...", _checksumElement.ToString(), Constants.ChecksumsUri);
                    }

                    ChecksumXml.Root.Elements(xmlns + "Checksum")
                                    .LastOrDefault()
                                    .AddAfterSelf(_checksumElement);

                    //Grab the stream so we can save to the checksums.xml file.
                    System.IO.Stream _stream = ChecksumsPackagePart.GetStream(System.IO.FileMode.Open, System.IO.FileAccess.Write);

                    if (args.Verbose)
                    {
                        System.Console.WriteLine(string.Format("Saving Package Part {0}...", Constants.ChecksumsUri));
                    }
                    ChecksumXml.Save(_stream, System.Xml.Linq.SaveOptions.None);

                    _stream.Dispose();
                }
                dacpac.Flush();
                dacpac.Close();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                throw ex;
            }

        }
    }
}

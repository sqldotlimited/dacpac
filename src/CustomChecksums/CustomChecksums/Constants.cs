﻿namespace CustomChecksums
{
    class Constants
    {
        public const string ModelXmlUri = "/model.xml";
        public const string OriginXmlUri = "/origin.xml";
        public const string PreDeployUri = "/predeploy.sql";
        public const string PostDeployUri = "/postdeploy.sql";
        public const string ChecksumsUri = "/checksums.xml";
        public const string SortedModelXmlUri = "/sortedmodel.doesnotexist";
        public const string xmlns = "http://schemas.microsoft.com/sqlserver/dac/Serialization/2012/02";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomChecksums
{
    class Program
    {
        /// <summary>
        /// Add's the Checksum Elements in a new file within the dacpac to they can be used for comparison's
        /// when building in a CI/CD environment. This helps by not needing the DacFx to go off and compare
        /// the Target and Source models if we know nothing has changed. 
        /// </summary>
        /// <param name="args">Used to pass in the command line arguments</param>
        static void Main(string[] args)
        {
            try
            {
                var options = new Options();

                if (CommandLine.Parser.Default.ParseArguments(args, options))
                {
                    if (string.IsNullOrEmpty(options.InputFile))
                    {
                        throw new System.ApplicationException("Error: No Input File");
                    }

                    System.IO.FileInfo fi = new System.IO.FileInfo(options.InputFile);

                    if (!fi.Exists)
                    {
                        throw new System.IO.FileNotFoundException(options.InputFile);
                    }

                    // Create the Checksum Element from the Origin.xml
                    CreateChecksumElementFromOrigin.Create(fi, options);

                    #region Checksum Pre Deploy File
                    if (options.ChecksumPreDeployFile)
                    {
                        System.Console.WriteLine("~ Checksum {0} ~", string.Format(Constants.PreDeployUri));
                        PreDeployChecksum.CreatePreDeployChecksumElement(fi, options);
                    }
                    #endregion Checksum Pre Deploy File

                    #region Checksum Post Deploy File
                    if (options.ChecksumPostDeployFile)
                    {
                        System.Console.WriteLine("~ Checksum {0} ~", string.Format(Constants.PostDeployUri));
                        PostDeployChecksum.CreatePostDeployChecksumElement(fi, options);
                    }
                    #endregion Checksum Post Deploy File

                    #region Remove Attributes, Sort Model and Checksum
                    if (options.ChecksumSortedModelXml)
                    {
                        System.Console.WriteLine("~ Checksum {0} ~", string.Format(Constants.SortedModelXmlUri));
                        SortModelXmlAndChecksum.RemoveSortChecksum(fi, options);
                    }
                    #endregion Remove Attributes, Sort Model and Checksum
                }
            }
            catch(System.ArgumentException ae)
            {
                System.Console.WriteLine(ae.Message);
                Environment.Exit(-2);
            }
            catch(System.IO.FileNotFoundException fnfe)
            {
                System.Console.WriteLine(fnfe.Message);
                Environment.Exit(-3);
            }
            catch(System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }
        }
    }
}

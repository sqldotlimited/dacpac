﻿using System.Linq;

namespace CustomChecksums
{
    class CreateChecksumElementFromOrigin
    {
        /// <summary>
        /// This method will assumes that the Checksums Element already exists in the dacpac. It will then create a custom checksums.xml
        /// part in the dacpac file.
        /// </summary>
        /// <param name="InputFile">FileInfo of the InputFile (dacpac)</param>
        /// <param name="args">Options</param>
        public static void Create(System.IO.FileInfo InputFile, Options args)
        {
            try
            {
                System.IO.Packaging.Package dacpac = System.IO.Packaging.Package.Open(InputFile.FullName, System.IO.FileMode.OpenOrCreate);

                System.Uri OriginUri = System.IO.Packaging.PackUriHelper.CreatePartUri(new System.Uri(Constants.OriginXmlUri, System.UriKind.Relative));
                System.IO.Packaging.PackagePart OriginPackagePart = dacpac.GetPart(OriginUri);

                System.Uri ChecksumsUri = System.IO.Packaging.PackUriHelper.CreatePartUri(new System.Uri(Constants.ChecksumsUri, System.UriKind.Relative));
                System.IO.Packaging.PackagePartCollection _parts;

                // Delete the part if it already exists in the dacpac.
                try
                {
                    _parts = dacpac.GetParts();

                    foreach (System.IO.Packaging.PackagePart _part in _parts)
                    {
                        if (_part.Uri.ToString() == Constants.ChecksumsUri)
                        {
                            if (args.Verbose)
                            {
                                System.Console.WriteLine(string.Format("Deleting Package Part {0}...", Constants.ChecksumsUri));
                            }
                            dacpac.DeletePart(ChecksumsUri);
                            break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }

                // Create an XDocument so we can grab the Checksums Element from the Origin package part.
                System.Xml.Linq.XDocument OriginXml = System.Xml.Linq.XDocument.Load(System.Xml.XmlReader.Create(OriginPackagePart.GetStream()));
                // Assign the xml name space correctly.
                System.Xml.Linq.XNamespace xmlns = Constants.xmlns;

                //Grab the Checksums Element so we can add it to our customs checksums.xml file later.
                if (args.Verbose)
                {
                    System.Console.WriteLine(string.Format("Getting XElement {0}...", xmlns + "Checksums"));
                }
                System.Xml.Linq.XElement ChecksumElement = OriginXml.Root.Elements(xmlns + "Checksums").FirstOrDefault();

                //Create the checksums.xml part
                if (args.Verbose)
                {
                    System.Console.WriteLine(string.Format("Creating Package Part {0}...", Constants.ChecksumsUri));
                }
                System.IO.Packaging.PackagePart ChecksumsPackagePart = dacpac.CreatePart(ChecksumsUri, System.Net.Mime.MediaTypeNames.Text.Xml);
                System.Xml.Linq.XDocument ChecksumsXml = System.Xml.Linq.XDocument.Parse(ChecksumElement.ToString());

                //Grab the stream so we can save to the checksums.xml file.
                System.IO.Stream _stream = ChecksumsPackagePart.GetStream(System.IO.FileMode.Open, System.IO.FileAccess.Write);

                if (args.Verbose)
                {
                    System.Console.WriteLine(string.Format("Saving Package Part {0}...", Constants.ChecksumsUri));
                }
                ChecksumsXml.Save(_stream, System.Xml.Linq.SaveOptions.None);

                _stream.Dispose();
                dacpac.Flush();
                dacpac.Close();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

        }

    }
}

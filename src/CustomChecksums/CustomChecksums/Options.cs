﻿namespace CustomChecksums
{
    /// <summary>
    /// Options Class that will output the available options when a user uses the help param.
    /// </summary>
    class Options
    {
        /// <summary>
        /// cmd/Powershell Option i or --input for the required input dacpac to read.
        /// </summary>
        /// <example>
        /// CustomChecksums.exe -i "C:\temp\example.dacpac"
        /// </example>
        [CommandLine.Option('i', "input", Required = true, HelpText = "Input dacpac to read")]
        public string InputFile { get; set; }

        /// <summary>
        /// cmd/Powershell Option --ChecksumPreDeployFile option to create a Checksum element for the predeploy.sql file inside the dacpac
        /// </summary>
        /// <example>
        /// CustomChecksums.exe -i "C:\temp\example.dacpac" --ChecksumPreDeployFile
        /// </example>
        [CommandLine.Option("ChecksumPreDeployFile", HelpText = "Use this option to create a Checksum element for the predeploy.sql file inside the dacpac")]
        public bool ChecksumPreDeployFile { get; set; }

        /// <summary>
        /// cmd/Powershell Option --ChecksumPostDeployFile option to create a Checksum element for the postdeploy.sql file inside the dacpac
        /// </summary>
        /// <example>
        /// CustomChecksums.exe -i "C:\temp\example.dacpac" --ChecksumPreDeployFile
        /// </example>
        [CommandLine.Option("ChecksumPostDeployFile", HelpText = "Use this option to create a Checksum element for the postdeploy.sql file inside the dacpac")]
        public bool ChecksumPostDeployFile { get; set; }

        /// <summary>
        /// cmd/Powershell Option --ChecksumSortedModelXml option to create a Checksum element for the model.xml file inside the dacpac.
        /// This will remove FileName and AssembleySymbolName Elements and then perform a sort on the model.xml file, grab the revised checksum and insert it 
        /// into a custom element within the dacpac
        /// </summary>
        /// <example>
        /// CustomChecksums.exe -i "C:\temp\example.dacpac" --ChecksumSortedModelXml
        /// </example>
        [CommandLine.Option("ChecksumSortedModelXml", HelpText = "Use this option to sort the model.xml file in memory, remove FileName and AssembleySymbolName atrributes and grab the checksum. The actual model.xml will be left alone.")]
        public bool ChecksumSortedModelXml { get; set; }

        /// <summary>
        /// cmd/Powershell Option v or --verbose to write the output to the console window during execution.
        /// </summary>
        /// <example>
        /// CustomChecksums.exe -i "C:\temp\example.dacpac" -v
        /// </example>
        [CommandLine.Option('v', "verbose", HelpText = "Write the output to the console window during execution.")]
        public bool Verbose { get; set; }

        /// <summary>
        /// Return the Usage Options available to a user within a cmd/powershell window
        /// </summary>
        /// <returns>Output to the console window to show help usage and options</returns>
        /// <example>CustomChecksums.exe --help</example>
        [CommandLine.HelpOption]
        public string GetUsage()
        {
            var help = new CommandLine.Text.HelpText
            {
                Heading = new CommandLine.Text.HeadingInfo("Custom Checksums Utility", "v 1.0"),
                Copyright = new CommandLine.Text.CopyrightInfo("SQL Dot Limited (07637715)", System.DateTime.Now.Year),
                AdditionalNewLineAfterOption = true,
                AddDashesToOption = true
            };

            help.AddOptions(this);
            return help;
        }
    }
}

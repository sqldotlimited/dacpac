﻿namespace CustomChecksums
{
    class Checksum
    {
        /// <summary>
        /// Calculates a checksum based on a stream.
        /// </summary>
        /// <param name="_stream">The Stream to Checksum.</param>
        /// <returns>Byte Array that can used as ToString("X2")</returns>
        public static byte[] CalculateChecksum(System.IO.Stream _stream)
        {
            if (_stream == null)
            {
                throw new System.ArgumentException("Stream is null");
            }

            return System.Security.Cryptography.HashAlgorithm.Create("System.Security.Cryptography.SHA256CryptoServiceProvider").ComputeHash(_stream);
        }

        public static byte[] CalculateChecksum(System.IO.MemoryStream _stream)
        {
            if (_stream == null)
            {
                throw new System.ArgumentException("MemoryStream is null");
            }

            return System.Security.Cryptography.HashAlgorithm.Create("System.Security.Cryptography.SHA256CryptoServiceProvider").ComputeHash(_stream);
        }
    }
}
